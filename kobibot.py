"""
KobiBot v0.1

woof.
"""
# --------------------------------------------------------------------------------------------------
#region Modules
#
import os
import sys
from datetime import datetime

import discord # Requires Python 3.6
# https://discordpy.readthedocs.io/en/latest/
# https://pypi.org/project/discord.py/
# https://realpython.com/how-to-make-a-discord-bot-python/

from lib.Log import Log
from lib.modules import KobiBotModules
import lib.settings
#endregion

# --------------------------------------------------------------------------------------------------
# Config Initialization
#
DEBUG = True
TOKEN = open(os.path.join(os.path.dirname(os.path.realpath(__file__)), ".token"), "r", encoding="utf-8").read().strip()

class KobiBot(KobiBotModules, discord.Client):
    """woof"""
    # --------------------------------------------------------------------------------------------------
    #region Init
    #
    def __init__(self):
        """
        __init__ [summary]
        """
        super().__init__()
        output = []
        output += [f"Discord.py v{discord.__version__}, {discord.__file__}"]
        output += [f"Python: v{sys.version_info.major}.{sys.version_info.minor}.{sys.version_info.micro}, {sys.executable}"]

        self.COMMANDS = {}
        self.COMMANDPREFIX = "!"

        self.REACTIONS = {}

        # Get the current working directory
        self.cwd = os.path.dirname(os.path.realpath(__file__))
        output += [f"File Directory: {self.cwd}\n"]

        # Load the Modules
        if hasattr(self, "LoadModules"):
            self.LoadModules(os.path.join(self.cwd, "modules"))

        Log(output, prefix="    - ")
    #endregion Init

    # --------------------------------------------------------------------------------------------------
    #region On Connect
    #
    async def on_ready(self):
        """on_ready"""
        output = []
        output += [f"Logged in as {self.user.name}({self.user.id})..."]

        # for guild in self.guilds:
        #     output += ["- {guild.name}"]

        Log(output)
    #endregion Connections

    # --------------------------------------------------------------------------------------------------
    #region On Events
    #
    async def on_message(self, message):
        """
        on_message [summary]

        Args:
            message ([type]): https://discordpy.readthedocs.io/en/latest/api.html#message
        """
        # Don't respond to ourselves
        if message.author == self.user:
            return

        # Parse the message
        if message.content.startswith(self.COMMANDPREFIX): # Is recognized as a command
            segments = message.content.split(" ", 1) # separate command from the rest of the string
            cmd = segments[0].lower()[1:] # strip the command prefix portion from the command

            # Run the associated command if it exists and pass the message to it.
            if cmd in self.COMMANDS:

                msg, reaction = self.COMMANDS[cmd](bot, message)
                if msg:
                    returned_message = await message.channel.send(msg)

                    # If the message is expected to respond to a reactions, see if  exists and register it.
                    if reaction and returned_message.id not in self.REACTIONS:
                        self.REACTIONS[returned_message.id] = reaction
                        # Need to store returned_message.id to reactions somewhere for when the bot is restarted

    async def on_reaction_add(self, reaction, user):
        """
        Called when a message the bot has sent has a reaction added to it. Similar to on_message_edit, if the message is not
        found in the Client.messages cache, then this event will not be called.

        Note: To get the message being reacted, access it via Reaction.message.

        Arguments:
            reaction {discord.Reaction} -- https://discordpy.readthedocs.io/en/latest/api.html#reaction
            user {discord.User} -- https://discordpy.readthedocs.io/en/latest/api.html#user
        """

        output = []
        output += [f"- Reaction: {reaction.emoji}, Message ID: #{reaction.message.id}: {reaction.message.content}\n"]
        output += [f"- User: {user.name}({user.id})\n"]

        # Loop registered reactions and pass to the necessary module reaction function.
        if reaction.message.id in self.REACTIONS:
            msg = self.REACTIONS[reaction.message.id](reaction, user)
            if msg:
                returned_message = await reaction.message.channel.send(msg) #pylint: disable=unused-variable

        Log(output)

      #endregion OnEvent

    # --------------------------------------------------------------------------------------------------
    #region Command Management
    #
    def RegisterCommand(self, command_text, command_function):
        """
        Register a command to the bot.

        Arguments:
            command_text {str} -- The text that this command will respond to.
            command_function {function} -- The function to run.
        """
        command_text = command_text.lower()
        if command_text not in self.COMMANDS:
            self.COMMANDS[command_text] = command_function
            Log(f"Added {command_text} -> {command_function}")
        else:
            Log(f"Command {command_text} already exists")

    def RegisterReaction(self, reaction, command_function):
        """
        RegisterReaction [summary]

        Args:
            reaction ([type]): [description]
            command_function ([type]): [description]
        """
        if reaction not in self.REACTIONS:
            self.REACTIONS[reaction] = command_function
            Log(f"Added {reaction} -> {command_function}")
        else:
            Log(f"Command {reaction} already exists")


    #endregion Command Management

# --------------------------------------------------------------------------------------------------
#region Main
if __name__ == '__main__':
    bot = KobiBot()
    bot.run(TOKEN)

# msg = "!test 1 2 3 4"
# # segments = message.content.split(" ", 1) # separate command from the rest of the message string
# segments = msg.split(" ", 1)

# args = []
# if len(segments) > 1: # !cmd args
#     # Split by space, strip out empty entries
#     filtered_args = filter(None, segments[1].split(' '))
#     args = list(filtered_args)

# return_msg = ", ".join(args)

# print(f"msg = {msg}")
# print(f"args = {args}")
# print(f"return_msg = {return_msg}")

#endregion Main
