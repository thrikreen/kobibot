"""
KobiBot unit tests
"""
# --------------------------------------------------------------------------------------------------
# Modules
#
import unittest
from datetime import datetime

import kobibot

# --------------------------------------------------------------------------------------------------
# Tests
#
class Test_KobiBot(unittest.TestCase):
    """
    [summary]
    """

    # --------------------------------------------------------------------------------------------------
    # GetTimestamp()
    #
    default_timestamp_format = "%H:%M:%S"
    full_timestamp_format = "%Y-%m-%d %H:%M:%S.%f"
    def test_GetTimestamp(self):
        """
        Test the returned timestamp is correct
        """
        source_timestamp = datetime.now().strftime(self.default_timestamp_format)
        target_timestamp = kobibot.GetTimestamp()
        self.assertEqual(source_timestamp, target_timestamp)

    def test_GetTimestampFormat(self):
        """
        Test the returned timestamp is in the correct custom format.
        """
        source_timestamp = datetime.now().strftime(self.full_timestamp_format)
        target_timestamp = kobibot.GetTimestamp(self.full_timestamp_format)
        self.assertEqual(source_timestamp, target_timestamp)

    def test_GetTimestampCustom(self):
        """
        Test the returned timestamp is in the correct custom format.
        """
        newyear_timestamp = datetime(2000, 1, 1, 0, 0, 0, 0) # Jan 1st, 2000, midnight
        source_timestamp = newyear_timestamp.strftime(self.default_timestamp_format)
        target_timestamp = kobibot.GetTimestamp(timestamp=newyear_timestamp)
        self.assertEqual(source_timestamp, target_timestamp)


    # --------------------------------------------------------------------------------------------------
    # Log()
    #
    log_message = "Debug Output"
    log_arg1 = "Arg1"
    log_arg2 = "Arg1"

    def test_Log(self):
        """
        Test the Log() output matches expected.
        """
        source_timestamp = datetime.now().strftime(self.default_timestamp_format)
        source_output = "[ {} ] test_Log(): {}".format(source_timestamp, self.log_message)
        output = kobibot.Log(self.log_message)
        self.assertEqual(source_output, output)

    def test_LogArgs(self):
        """
        Test the Log() output matches expected with args
        """
        source_timestamp = datetime.now().strftime(self.default_timestamp_format)
        source_output = "[ {} ] test_LogArgs({}, {}): {}".format(source_timestamp, self.log_arg1, self.log_arg2, self.log_message)
        output = kobibot.Log(self.log_message, self.log_arg1, self.log_arg2, timestamp=self.default_timestamp_format)
        self.assertEqual(source_output, output)

    def test_LogFunctionName(self):
        """
        Test the Log() output matches expected with args
        """
        source_timestamp = datetime.now().strftime(self.default_timestamp_format)
        source_output = "[ {} ] test_Log(): {}".format(source_timestamp, self.log_message)
        output = kobibot.Log(self.log_message, functionname="test_Log")
        self.assertEqual(source_output, output)

    def test_LogPrefix(self):
        """
        Test the Log() output matches expected with args
        """
        source_timestamp = datetime.now().strftime(self.default_timestamp_format)
        source_output = "[ {} ] Test_KobiBot.test_LogPrefix(): {}".format(source_timestamp, self.log_message)
        output = kobibot.Log(self.log_message, prefix="Test_KobiBot")
        self.assertEqual(source_output, output)

    def test_LogCustomTimestamp(self):
        """
        Test the Log() output matches expected with a custom timestamp format.
        """
        source_timestamp = datetime.now().strftime(self.full_timestamp_format)
        source_output = "[ {} ] test_LogCustomTimestamp(): {}".format(source_timestamp, self.log_message)
        output = kobibot.Log(self.log_message, timestamp=self.full_timestamp_format)
        self.assertEqual(source_output, output)








# --------------------------------------------------------------------------------------------------
# Main
#
if __name__ == '__main__':
    unittest.main()
