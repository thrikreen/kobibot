"""
[summary]
"""
# --------------------------------------------------------------------------------------------------
#region Modules
#
import os
import sys

from lib.Log import Log #pylint: disable=import-error
#endregion Modules

# --------------------------------------------------------------------------------------------------
#region Functions
#
def Command(bot, message):
    """
    Command [summary]

    Args:
        bot (discord.Client): https://discordpy.readthedocs.io/en/latest/api.html#client
        message (discord.Message): https://discordpy.readthedocs.io/en/latest/api.html#message
    """
    output = []
    msg = ""
    reaction_function = None

    output += [f"Bot: {bot}"]

    # Split the command from the message contents (if any)
    segments = message.content.split(" ", 1) # separate command from the rest of the message string
    args = []
    if len(segments) > 1: # !cmd args
        args = list(filter(None, segments[1].split(' '))) # Separate by space, strip out empty entries

        if args:
            if args[0].lower() == "keyword":
                # do something
                pass

        if args: # Debug
            argOutput = ", ".join(args)
            output += [f"{argOutput}"]

        # Return the message to output, along with the reaction function if applicable
        msg += f"{message.author.mention}: {argOutput} <:eskie:934974534547820634>"

    else: # just the command trigger itself
        # Do something
        pass

    output += [msg]
    output += [f"ChannelID: {message.channel.id}"]
    output += [f"GuildID: {message.guild.id}"]
    Log(output)

    # Return the response msg
    # If we want to process reactions to the message, pass the reaction function, otherwise, leave reaction as the default of 'None'.
    # TODO: create a msg object to set the target channel/user to send to, instead of just a msg to the originating channel #pylint: disable=fixme
    reaction_function = Reaction
    return(msg, reaction_function)

def Reaction(reaction, user):
    """
    Template Reaction

    Arguments:
        reaction {[type]} -- [description]
        user {[type]} -- [description]
    """
    output = []
    msg = ""

    if reaction.emoji.id == 934974534547820634:
        msg = f"{user.mention}: woof! <:eskie:934974534547820634>"

    output += [msg]
    Log(output)

    return(msg)
#endregion Functions

# --------------------------------------------------------------------------------------------------
#region Register
#
def Register(bot):
    """
    Registers this module and command with the supplied DiscordBot.

    Arguments:
        bot {DiscordBot} -- The bot(module) to register this command with.
    """

    bot.RegisterCommand("test", Command)
    # bot.RegisterReaction("", Reaction)

#endregion
