"""kobibot/lib/modules"""
# --------------------------------------------------------------------------------------------------
#region Modules
#
import os
import sys
from datetime import datetime
import importlib

from lib.Log import Log #pylint: disable=import-error
#endregion

class KobiBotModules():
    """
    KobiBotModules [summary]
    """
    # --------------------------------------------------------------------------------------------------
    #region Init
    #
    def __init__(self):
        """init()"""
        super().__init__()
        self.MODULES = {}
    #endregion Init

    # --------------------------------------------------------------------------------------------------
    #region Functions
    #
    def LoadModules(self, module_root):
        """
        Loads the various bot modules from the supplied subdirectory.

        Arguments:
            module_root {str} -- [description]
        """
        output = []
        module_files = []

        # Get the current directory
        output += [f"Module Directory: {module_root}"]

        # Collect all the module files
        for root, dirs, files in os.walk(module_root): #pylint: disable=unused-variable
            # Collect valid files in root folder
            for f in files:
                if f[0] in ['.', '_'] or os.path.splitext(f)[1].lower() != ".py": # ignore files with names starting with . or _ (hidden), or are not .py files
                    continue
                module_filepath = os.path.join(root, f) # get the abs path of the module
                module_files.append(module_filepath)

        # Load the modules and run their respective register command with the bot
        output += [f"Summary: Found {len(module_files)} modules"]
        for module_path in module_files:
            relative_path = os.path.dirname(module_path).replace(module_root, "") # relative path to the root
            if relative_path == "":
                relative_path = "\\"
            module_name = os.path.splitext(os.path.basename(module_path))[0]

            # Try to load the module and if it has a RegisterCommand() function, run it with this Bot
            module = None
            try:
                # Check which Python env we're working in
                if sys.version_info.major == 3 and sys.version_info.minor >= 5: # Python 3.5+
                    spec = importlib.util.spec_from_file_location(module_name, module_path)
                    module = importlib.util.module_from_spec(spec)
                    spec.loader.exec_module(module)
                    output += [f"- {module_path} -> Loaded '{module_name}'"]

                # elif sys.version_info.major == 3 and sys.version_info.minor >= 0: # Python 3.0+
                #     module = importlib.machinery.SourceFileLoader(module_name, module_path).load_module() #pylint: disable=deprecated-method,no-value-for-parameter
                #     # output_text.append("<3.5: Loaded module: {}".format(module_name))

                # elif sys.version_info.major == 2: # Python 2.x
                #     module = imp.load_source(module_name, module_path)
                #     # reload(module) #type: ignore #pylint: disable=undefined-variable
                #     # output_text.append("2.x: Loaded module: {}".format(module_name))

                else:
                    output += [f"[ ERROR ] - {module_path} -> Unable to load '{module_name}': ???"]

            except Exception as e: #pylint: disable=broad-except
                output += [f"[ ERROR ] - {module_path} -> Unable to load '{module_name}': {e}"]

            if module:
                if hasattr(module, "Register"):
                    module.Register(self)

        output += [""]
        Log(output, module_root)
        return

    #endregion Functions
