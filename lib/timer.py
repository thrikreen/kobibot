"""timer.py"""
# ----------------------------------------------------------------------------------------------------------------------
#region Modules
#
import os
import sys
import random
import time
from datetime import datetime
#endregion Modules

# ----------------------------------------------------------------------------------------------------------------------
#region Global
#
TIMERS = {}
#endregion

# ----------------------------------------------------------------------------------------------------------------------
#region Functions
#
# start_time = datetime.now()
# elapsed_time = datetime.now() - start_time
# display_time = "{:02d}:{:02d}:{:02d}".format(elapsed_time.seconds/3600, (elapsed_time.seconds/60)%60, elapsed_time.seconds%60)

def start(key):
    """
    Starts a timer of the current time with an identifier of 'key'.

    Args:
        key (str): Label key of this timer to use. Will overwrite an existing key.
    """
    TIMERS[key] = datetime.now()
    return TIMERS[key]

def end(key):
    """
    Gets the elapsed time for the timer 'key' (if found).

    Args:
        key (str): Label key of this timer to use.

    Returns:
        [timedelta]: The elapsed time from the start time, in seconds, if found. Returns None if that timer key was not found.
    """
    if key in TIMERS:
        return datetime.now() - TIMERS[key]
    return None

def clear(key):
    """
    Removes this timer key from the global list.

    Args:
        key (str): [description]
    """
    if key in TIMERS:
        del TIMERS[key]

def displaytime(key, show_ms=False):
    """
    Displays the elapsed time for the timer key, if found.

    Args:
        key (str): The timer key to retrieve, if found.
        show_ms (bool, optional): Display milliseconds or not. Defaults to False.

    Returns:
        [str]: The elapsed time in the format of HH:MM:SS.ms if the timer key was found. Otherwise returns None.
    """
    display_time = None
    if key in TIMERS:
        # elapsed_time = datetime.now() - TIMERS[key]
        # display_time = "{:02d}:{:02d}:{:02d}".format(int(elapsed_time.total_seconds()/3600), int((elapsed_time.total_seconds()/60)%60), int(elapsed_time.total_seconds()%60))
        if show_ms:
            # display_time += ".{:02d}".format(int(elapsed_time.microseconds))
            display_time = GetElapsedTime(TIMERS[key], timestamp_format="%H:%M:%S.%f")
        else:
            display_time = GetElapsedTime(TIMERS[key], timestamp_format="%H:%M:%S")

    return display_time

def GetElapsedTime(initial_time, now=datetime.now(), timestamp_format="%H:%M:%S.%f"):
    """
    Returns a datetime string calculating the elapsed time from initial_time to now. Milliseconds rounds to 3 digits.
    Default format is "%H:%M:%S.%f", i.e. 21 hours, 4 minutes and 35.767209 seconds will display as 21:04:35.767

    Args:
        initial_time (datetime): The starting time to calculate from.
        now (datetime, optional): The ending time to calculate to. Defaults to datetime.now().
        timestamp_format (str, optional): The string format for how to output the time, supports %Y, %D, %H, %M, %S and %f. Defaults to "%H:%M:%S.%f"

    Returns:
        str: The elapsed date and time in the format specified.
    """
    # Get the elapsed time (in seconds)
    elapsed_time = now - initial_time

    # Calculate the years, months, days, hours, minutes, seconds, milliseconds to 2 decimal places?
    # [q, r] = divmod(x, y)
    years = divmod(elapsed_time.total_seconds(), 31536000)
    days = divmod(years[1], 86400)
    hours = divmod(days[1], 3600)
    minutes = divmod(hours[1], 60)
    seconds = divmod(minutes[1], 1)
    milliseconds = elapsed_time.microseconds
    milliseconds = str(milliseconds)[0:3] # get the first 3 digits

    # Output the results based on the timestamp format requested - defaults to "%H:%M:%S.%f"
    # https://docs.python.org/3/library/datetime.html#strftime-and-strptime-format-codes
    timestamp_format = timestamp_format.replace("%Y", "{:.0f}".format(years[0]))
    timestamp_format = timestamp_format.replace("%D", "{:.0f}".format(days[0]))
    timestamp_format = timestamp_format.replace("%H", "{:02.0f}".format(hours[0]))
    timestamp_format = timestamp_format.replace("%M", "{:02.0f}".format(minutes[0]))
    timestamp_format = timestamp_format.replace("%S", "{:02.0f}".format(seconds[0]))
    timestamp_format = timestamp_format.replace("%f", "{}".format(milliseconds))

    # elapsed_time_string = "{:02d}:{:02d}:{:02d}.{:02d}".format(hours, mins, seconds, int(elapsed_time.microseconds))
    return timestamp_format
#endregion Functions

# ----------------------------------------------------------------------------------------------------------------------
#region Main
#
def main():
    """main"""
    mytimer = "mytimer"
    timerange = 5

    start_time = start(mytimer)
    time.sleep(random.randrange(1, timerange))
    end_time = end(mytimer)

    print("Elapsed Time: {} -> {}".format(start_time, end_time))
    print("Elapsed Time: {}".format(GetElapsedTime(start_time)))

    # print("convertdisplaytime(): {}".format(convertdisplaytime(elapsed_time)))
    print("displaytime(): {}".format(displaytime(mytimer)))

    clear(mytimer)
    print("clear(): {}".format(displaytime(mytimer)))

if __name__ in ["__main__"]:
    main()

#endregion Main
