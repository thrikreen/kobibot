"""
Logging Stuff
"""
import os
import sys
# import inspect
import random
import logging # https://docs.python.org/3/library/logging.html

from datetime import datetime

DEBUG = True
GLOBAL = {}
GLOBAL_FLAGS = ["prefix", "suffix", "logfile", "endnewline"]
PREFIX, SUFFIX, LOGFILE, ENDNEWLINE = GLOBAL_FLAGS


# ----------------------------------------------------------------------------------------------------------------------
# region Main
#
def SetGlobalLogProperties(key, value):
    """
    Sets some global properties for use with the Log() function, saves having to define them on every call.

    Args:
        key (const): The property key to set:
            PREFIX: Sets the default prefix to use on every log message line (when using a list). Useful to prefix with a tab
            SUFFIX:
            LOGFILE: Path to a log file to write all error messages to
            ENDNEWLINE: Add an extra newline at the end of every message for spacing.
        value ([type]): [description]
    """
    GLOBAL[key] = value

def Log(message, *argv, **kwargv):
    """
    Writes a timestamped message to the standard output (console, listener). Will recognize the calling function name.
    Use key=value to control output options (i.e. colour text for a QTextbox, etc.).

    Output format:
        [ HH:MM:SS ] <stack.>FunctionName(arg, <argN>): <message>

    Usage:
        message {string/list}   -- The message to output. Accepts string or a list of strings. If a list, it will automatically separate each item with a newline on output.
        *argv {any}             -- Any args will get added in brackets after the function name, useful to see what is getting passed to the calling function.

    Keyword Arguments:
        timestamp {str}     -- Override the default timestamp format (default: '%H:%M:%S')
        logfile {str}       -- If set, will also write the output that is displayed to the target file. Should be a full path and filename. (default: '')

        prefix {str}        -- Will add a prefix to the function name, useful to differentiate multiple calls. (default: '')
        functionname {str}  -- Will override the functionname with this instead, useful for environments like IronPython where the ability to inspect the callstack is not available. (default: '')
        functionprefix {str}-- Prepends the function name with this (useful for separating by object calls)

        prefix {str}        -- If set, when joining a list, it will prefix the elements with this (useful for indenting).
        suffix {str}        -- If set, when joining a list, it will end the elements with this (useful for outputting to a single line using a delimiter instead of \n as normal).

        verbose {bool}      -- If there's a global toggle controlling all output, use this to override it.

    Returns:
        [string] -- The final log content that was outputted with the fancy timestamp and other stuff.
    """
    output = ""
    timestamp_format = '%H:%M:%S'
    timestamp = datetime.now().strftime(timestamp_format)

    # --------------------------------------------------------------------------
    # Get the function name that called Log()
    # NOTE: _getframe() not supported in IronPython, as not available in the inspect module either, need to resort to manually sending it with the message

    # while frame and frame.f_code.co_filename == f:
    #     frame = frame.f_back

    # Log(function) -> Log(module) -> class function -> class obj ->

    function_name = None
    callstack = []
    # callstack_type = []
    selfstack = []

    # Get the current callstack
    try:
        frame = sys._getframe() #pylint: disable=protected-access
        while frame:
            if frame.f_code.co_name == "<module>":
                # Get the module filename
                module_filename = os.path.basename(frame.f_code.co_filename)
                # module_filename = os.path.splitext(module_filename)[0]
                callstack.append(module_filename)
                break
            else:
                callstack.append(frame.f_code.co_name)

            # Get the class name of the object if it exists
            if "self" in frame.f_locals:
                if frame.f_locals['self'].__class__.__name__ not in selfstack:
                    selfstack.append(frame.f_locals['self'].__class__.__name__)

            frame = frame.f_back # get previous frame in the stack
    except Exception as e: #pylint: disable=broad-except
        # Should use functionname="name" as a backup
        print(f"[ ERROR ] {e}")
    if callstack:
        callstack.reverse()
        callstack = callstack[:-1] # Remove the last element, should be this function, Log(), we want the preceding function (or module) that called Log() instead

        # Get the function name
        if selfstack: # Called from a class
            # if "__init__" in callstack:
            #     callstack.remove("__init__")
            if len(callstack) > 1: # Get the last entry, as it should be the one that called Log()
                callstack = [callstack[-1]]
            callstack.insert(0, selfstack[0]) # prefix class name
            function_name = ".".join(callstack) # class_name.function_name()

        else: # Module
            if len(callstack) > 1:
                function_name = callstack[-1]
            else:
                function_name = callstack[0] + "<module>"

        # if callstack: print("    Callstack: {}".format(callstack))
        # if selfstack: print("    selfstack: {}".format(selfstack))

    # --------------------------------------------------------------------------
    # Options
    #
    if 'timestamp' in kwargv or 'timestamp' in GLOBAL: # Override the default timestamp format
        timestamp_format = kwargv.get('timestamp') if 'timestamp' in kwargv else GLOBAL['timestamp'] if 'timestamp' in GLOBAL else '%H:%M:%S'
        timestamp = datetime.now().strftime(timestamp_format)
    if 'functionname' in kwargv: # override the function name, i.e. IronPython has no sys.getframe() so one has to set it manually
        function_name = kwargv.get('functionname')
    if 'functionprefix' in kwargv:
        function_name = kwargv.get('functionprefix') + "." + function_name

    # --------------------------------------------------------------------------
    # Display the args passed to the originating function call, if supplied
    #
    argv_output = ""
    if argv:
        argv_output = ", ".join(str(arg) for arg in argv)

    # --------------------------------------------------------------------------
    # Display output to console
    #
    prefix = kwargv['prefix'] if 'prefix' in kwargv else GLOBAL[PREFIX] if PREFIX in GLOBAL else ""
    suffix = kwargv['suffix'] if 'suffix' in kwargv else GLOBAL[SUFFIX] if SUFFIX in GLOBAL else "\n"

    if isinstance(message, list):
        formatted_message = []
        if len(message) > 1:
            # separator = kwargv['separator'] if 'separator' in kwargv else GLOBAL['separator'] if 'separator' in GLOBAL else ""
            # for i in range(len(message)): #pylint: disable=consider-using-enumerate
            #     formatted_message.append(prefix + message[i])
            for i, s in enumerate(message):
                message[i] = prefix + s
            formatted_message = message
        else:
            for msg in message:
                formatted_message.append(msg)

        # output = "[ {} ] {}({}):{}{}".format(timestamp, function_name, argv_output, suffix if len(formatted_message) > 1 else " ", suffix.join(formatted_message))

        if len(formatted_message) > 1: # Multiple lines to be joined by \n, no need to put a preceding space
            output = f"{suffix}{suffix.join(formatted_message)}"
        else:
            output = f" {suffix.join(formatted_message)}"

    elif isinstance(message, str):
        output = "\n" if "\n" in message else " " + f"\n{message}"

    else:
        output = f"{message}"

    output = f"[ {timestamp} ] {function_name}({argv_output}):{output}"

    if ENDNEWLINE in GLOBAL and GLOBAL[ENDNEWLINE]:
        output += "\n"

    if DEBUG or ('verbose' in kwargv and kwargv.get('verbose')): # Use the global DEBUG to control output, or force it using the keyword
        print(output)

    # --------------------------------------------------------------------------
    # Output file
    #
    if 'logfile' in kwargv:
        with open(kwargv["logfile"], "a", encoding="utf-8") as output_logfile:
            output_logfile.write(output)
            output_logfile.close()
    if LOGFILE in GLOBAL:
        with open(GLOBAL[LOGFILE], "a", encoding="utf-8") as output_logfile:
            output_logfile.write(output)
            output_logfile.close()

    return output

def GetLogFilename(filename, ext, userdir=True, foldername=None, timestamp=True, verbose=False):
    """
    Generates the log filename and path, based on the current date and target output directory.

    Args:
        filename ([type]): [description]
        ext ([type]): [description]
        userdir (bool, optional): Defaults the log output to the current user directory. Defaults to True.
        foldername ([type], optional): [description]. Defaults to None.
        timestamp (bool, optional): [description]. Defaults to True.
        verbose (bool, optional): [description]. Defaults to False.

    Returns:
        [type]: [description]
    """
    output = []

    output += [f"App Root: {os.path.dirname(sys.argv[0])}"]

    logfilename = ""
    if timestamp:
        timestamp = datetime.today().strftime('%Y-%m-%d_%H-%M-%S')
        logfilename = f"{filename}_{timestamp}.{ext}"
    else:
        timestamp = None
        logfilename = f"{filename}.{ext}"
    output += [f"logfilename: {logfilename}"]

    if userdir:
        # Save in the user directory
        # userprefs_filepath = os.path.join(os.path.expanduser("~"), company, application_name, prefs_filename)
        rootpath = os.path.expanduser("~")
    else:
        # default to application folder
        rootpath = os.path.dirname(sys.argv[0])

    if foldername:
        logfilename = os.path.join(rootpath, foldername, logfilename)
    else:
        logfilename = os.path.join(rootpath, logfilename)

    if verbose:
        Log(output, userdir, foldername, filename, timestamp)
    return logfilename
#endregion

# ----------------------------------------------------------------------------------------------------------------------
# region Classes
#
class TestClass():
    """testclass"""
    def __init__(self, *argv, **kwargv):
        """TestClass.init()"""
        self.name = self.__class__.__name__
        Log("__init__", *argv, **kwargv)

    def Log(self, message, *argv, **kwargv):
        """TestClass.Log()"""
        Log(message, *argv, **kwargv)
        # [ 03:01:49 ] TestClass.__init__():
        # __init__

    def DoSomething(self):
        """TestClass.DoSomething()"""
        output_text = []
        x = random.randrange(0, 1000)
        y = random.randrange(0, 1000)
        z = random.randrange(0, 1000)
        r = x + y + z

        output_text.append(f"TestClass.DoSomething.Log.Log: single message. result = {r}")
        self.Log(output_text, x, y, z)

        output_text.append("TestClass.DoSomething.Log.Log: in class, w/list")
        self.Log(output_text, x, y, z, forceoutput=True)

        output_text.append("TestClass.DoSomething.Log: in class, direct, w/list and prefix")
        Log(output_text, x, y, z, prefix="    - ")

        return r
#endregion Classes

# ----------------------------------------------------------------------------------------------------------------------
# region Main
#
def main2():
    """main2"""
    output = []

    output += ["main2()"]
    output += ["and stuff"]
    output += ["and more stuff"]

    Log(output, 1, 2, 3, None, "asdf", prefix="    ")
    # [ 03:00:23 ] main2(1, 2, 3, None, asdf):
    #     main2()
    #     and stuff
    #     and more stuff

    Log(output, suffix=";")
    # [ 03:01:49 ] main2():;    main2();    and stuff;    and more stuff




def main():
    """main()"""

    Log("main()")
    # [ 03:00:23 ] main():
    # main()

    main2()

    # Object
    obj = TestClass()
    obj.DoSomething()

if __name__ in ["__main__", "<module>"]:

    Log("<module>")
    # [ 03:00:23 ] Log.py<module>():
    # <module>

    main()

#endregion Main
