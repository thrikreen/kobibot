"""
Container Module for holding global settings and variables.
"""
from operator import truediv
import os
import sys
import json
from datetime import datetime

import pymysql

# from lib.Log import Log #pylint: disable=import-error

# ----------------------------------------------------------------------------------------------------------------------
#region Global Variables
#

#endregion Global Variables

# ------------------------------------------------------------------------------
#region Functions - MySQL
#
def GetSettings(serverid, key, overridejson=None):
    """
    GetSettings [summary]

    Args:
        serverid ([type]): [description]
        key ([type]): [description]
        overridejson ([type], optional): [description]. Defaults to None.

    Returns:
        [type]: [description]
    """
    content = None





    return content

def SaveSettings(serverid, key, value, overridejson=None):
    """
    SaveSettings [summary]

    Args:
        serverid ([type]): [description]
        key ([type]): [description]
        value ([type]): [description]
        overridejson ([type], optional): [description]. Defaults to None.
    """

    pass


#endregion Functions - MySQL

# ------------------------------------------------------------------------------
#region Functions - JSON
#
def LoadJson(filepath, template_config=None, content=None, verbose=False):
    """
    Loads the specified .JSON file, creates the file based on the contents of template if set.

    Args:
        filepath (str): Full filepath of the .JSON file to load.
        template (dictionary, optional): If the file is not found, create a new one using this as the template format. Defaults to None.

    Returns:
        dictionary: [description]
    """
    output = []
    config = {}

    if verbose:
        print(f"Config: {filepath}")

    if content:
        try:
            config = json.loads(content)
        except Exception as e: #pylint: disable=broad-except
            print(f"[ERROR] Error parsing {content}: {e}")
    else:
        # ------------------------------------------------------------------------------
        # File not found at location, create
        #
        if not os.path.exists(filepath):
            if template_config: # Template supplied, create it under a template_config header.
                if verbose:
                    print(f"File not found, creating new template at: {filepath}")

                # Make path if it doesn't exist
                if not os.path.exists(os.path.dirname(filepath)):
                    os.makedirs(os.path.dirname(filepath))

                config = template_config
                with open(filepath, 'w', encoding="utf-8") as f:
                    json.dump(config, f, indent=4)
            else: # No template, skip
                if verbose:
                    print("File not found, no template supplied.")
                config = template_config

        # ------------------------------------------------------------------------------
        # Load config
        else:
            try:
                with open(filepath, encoding="utf-8") as f:
                    config = json.load(f)
            except Exception as e: #pylint: disable=broad-except
                print(f"[ERROR] Error reading {filepath}: {e}")

    # ------------------------------------------------------------------------------
    # Display contents of read
    if verbose:
        output += [f"Dumping {filepath}:"]
        output += [json.dumps(config, indent=4, sort_keys=False)]
        print("\n".join(output))

    return config

def SaveJson(filepath, contents, indent=4, verbose=False):
    """
    Saves the dictionary contents as the specified file.

    Args:
        filepath (str): Full path to the target file to write to. If set to None, just returns the JSON contents as a string.
        contents (dict): The dictionary contents to write to file.
        indent (int, optional): JSON indent, use None for compact default. Defaults to 4 because we like being able to read it later.
        verbose (bool, optional): Show stuff. Defaults to False.

    Returns:
        [type]: [description]
    """
    output = []
    result = False

    if filepath:
        # Create directory if not found
        if not os.path.exists(filepath):
            if verbose:
                print(f"File not found, creating new file at: {filepath}")
            if not os.path.exists(os.path.dirname(filepath)):
                os.makedirs(os.path.dirname(filepath))

        # Save the JSON to file
        try:
            with open(filepath, 'w', encoding="utf-8") as f:
                json.dump(contents, f, indent=indent)
                result = True
        except Exception as e: #pylint: disable=broad-except
            print(f"[ERROR] Saving to: {filepath}, {e}")
    else:
        result = json.dumps(contents, indent=indent)

    if verbose:
        print("\n".join(output))

    return result
#endregion Functions - JSON

def ConnectToDatabase(dbsettings):
    """
    ConnectToDatabase [summary]

    Args:
        dbsettings ([type]): [description]

    Returns:
        [type]: [description]
    """
    # Initialize Connection to database
    # user=None,  # The first four arguments is based on DB-API 2.0 recommendation.
    # password="",
    # host=None,
    # database=None,
    # db = pymysql.connect(mysqlserver, mysqluser, mysqlpassword, mysqldatabase)
    db = pymysql.connect(
        host=dbsettings["host"],
        user=dbsettings["user"],
        password=dbsettings["password"],
        database=dbsettings["database"]
        )
    cursor = db.cursor()

    # ChannelID = 524633260874006539
    GuildID = 524633260270157826
    config = {}
    config["param1"] = "blah"
    config["param2"] = "blah2"
    content = SaveJson(None, config)

    # Save New Element
    # query = f"INSERT INTO server(serverid, config) VALUES('{GuildID}', '{content}')"
    #         INSERT INTO `server`(`serverid`, `config`) VALUES ('524633260270157826', '{}');

    # Update Existing Element
    query = f"UPDATE server SET config='{content}' WHERE SERVERID='{GuildID}'"

    print(f"Query: {query}")
    try:
        cursor.execute(query)
    except Exception as e: #pylint: disable=broad-except
        print(f"[ERROR] MySQL: {e}")

    # Get Element
    query = f"SELECT * FROM `server` WHERE SERVERID='{GuildID}'"
    try:
        cursor.execute(query)
        results = cursor.fetchall()
        for row in results:
            print(f"{row}")
    except Exception as e: #pylint: disable=broad-except
        print(f"[ERROR] MySQL: {e}")

    return cursor



# --------------------------------------------------------------------------------------------------
#region Main
#
print(f"Settings.__name__: {__name__}")
if __name__ in ['lib.settings', "__main__"]:

    # Load config
    cwd = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
    cwd = os.path.join(cwd, ".database")
    print(f"DB prefs: {cwd}")

    db_settings = LoadJson(cwd)
    ConnectToDatabase(db_settings)


#endregion Main
